/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.web.courses.webapp.model;

import java.util.List;

/**
 *
 * @author Regan Shakya <regan@moco.com.np>
 */
public interface CourseIdeaDAO {
    boolean add(CourseIdea idea);
    
    List<CourseIdea> findAll();
    
    CourseIdea findBySlug(String slug);
}
